package stepdefinitions;

import java.io.File;
import java.io.IOException;
import java.util.Properties;


import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.google.common.io.Files;

import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;



import factory.DriverFactory;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.AccountPage;
import pages.HomePage;
import pages.LoginPage;
import utils.CommonUtils;
import utils.ConfigReader;
import utils.ElementUtils;

public class Login {
	
	 WebDriver driver;
	private LoginPage loginPage;
	private AccountPage accountPage;
	private CommonUtils commonUtils;
	private ElementUtils elementutils;
	@Given("User navigates to login page")
	public void user_navigates_to_login_page() throws IOException {
		
	
		

		driver = DriverFactory.getDriver();
		HomePage homePage = new HomePage(driver);
		homePage.clickOnMyAccount();
		loginPage = homePage.selectLoginOption();
		
		
		  
		
		
		
	
	}
	
	@When("^User enters valid email address (.+) into email field$")
	public void User_enters_valid_email_address_into_email_field(String emailText) throws IOException {
		
		loginPage.enterEmailAddress(emailText);
		
		
	}
	
	@And("^User enters valid password (.+) into password field$")
	public void user_enters_valid_password_into_password_field(String passwordText) throws IOException {
		
	    
		loginPage.enterPassword(passwordText);
		
	}

	@And("User clicks on Login button")
	public void user_clicks_on_login_button() throws IOException {
	    
		accountPage = loginPage.clickOnLoginButton();
		
	}

	@Then("User should get successfully logged in")
	public void user_should_get_successfully_logged_in() throws IOException {
		
		Assert.assertTrue(accountPage.displayStatusOfEditYourAccountInformationOption());
	    
		
		 
	
	}

	@When("User enters invalid email address into email field")
	public void user_enters_invalid_email_address_into_email_field() throws IOException {
			    
		commonUtils = new CommonUtils();
		
		loginPage.enterEmailAddress(commonUtils.getEmailWithTimeStamp());
		
		
	
	}

	@When("User enters invalid password {string} into password field")
	public void user_enters_invalid_password_into_password_field(String invalidPasswordText) {
	    
		loginPage.enterPassword(invalidPasswordText);
		
	}

	@Then("User should get a proper warning message about credentials mismatch")
	public void user_should_get_a_proper_warning_message_about_credentials_mismatch() {
	  
		Assert.assertTrue(loginPage.getWarningMessageText().contains("Warning: No match for E-Mail Address and/or Password."));
		
		
	}

	@When("User dont enter email address into email field")
	public void user_dont_enter_email_address_into_email_field() {
		
		loginPage.enterEmailAddress("");
	    
	}

	@When("User dont enter password into password field")
	public void user_dont_enter_password_into_password_field() {
	    
		loginPage.enterPassword("");
		
	}
	

}
